from django.contrib import admin
from django.urls import path, include

admin.site.site_header = 'Apolo'
admin.site.site_title = 'Apolo'
admin.site.index_title = 'Apolo admin'
admin.empty_value_display = 'N/A'

urlpatterns = [
    path('admin_tools/', include('admin_tools.urls')),
    path('select2/', include('django_select2.urls')),
    # path('', include('material.frontend.urls')),
    # path('', include(frontend_urls)),
    # path('login/', views.LoginView.as_view(), name='login'),
    # path('logout/', views.LogoutView.as_view(), name='logout'),
    path('admin/', admin.site.urls),
    path('apolo/', include('apolo.urls', namespace='apolo')),
]

from django.conf import settings
from django.conf.urls import include, url

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

# pylint: skip-file

from .settings import *

INSTALLED_APPS += [
    'behave_django',
    'django_extensions',
    'debug_toolbar',
]

MIDDLEWARE = [
                 'debug_toolbar.middleware.DebugToolbarMiddleware',
             ] + MIDDLEWARE

DEBUG = os.getenv('DEBUG', 'True') == 'True'

ALLOWED_HOSTS += ['localhost']

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

MEDIA_ROOT = BASE_DIR / 'apolo/tests/fixtures'

FIXTURE_DIRS = [
    MEDIA_ROOT
]

FIXTURE_DEV = [
    'plano.json',
    'user.json',
]

INTERNAL_IPS = ['127.0.0.1', ]

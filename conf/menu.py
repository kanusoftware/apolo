"""
This file was generated with the custommenu management command, it contains
the classes for the admin menu, you can customize this class as you want.

To activate your custom menu add the following to your settings.py::
    ADMIN_TOOLS_MENU = 'apolo.menu.CustomMenu'
"""

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from admin_tools.menu import items, Menu
from django.utils.translation import ugettext_lazy as _


class CustomMenu(Menu):
    """
    Custom Menu for apolo admin site.
    """

    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem('Inicio', reverse('admin:index')),
            # items.Bookmarks(),
            items.MenuItem(
                title='Lançamentos',
                url=reverse('admin:apolo_extrato_changelist')
            ),
            items.MenuItem(
                title='Beneficiários',
                url=reverse('admin:apolo_beneficiario_changelist')
            ),
            items.ModelList(
                'Relatórios',
                [
                    'apolo.models.Categoria',
                    'apolo.models.Plano',
                    'apolo.models.Pessoa',
                    'apolo.models.Pessoa',
                    'django.contrib.auth.models.User',
                    'django.contrib.auth.models.Group',
                ]
            ),
            items.ModelList(
                'Cadastros',
                [
                    'apolo.models.Categoria',
                    'apolo.models.Plano',
                    'apolo.models.Pessoa',
                    'apolo.models.Pessoa',
                    'django.contrib.auth.models.User',
                    'django.contrib.auth.models.Group',
                ]
            ),
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomMenu, self).init_with_context(context)

server {
    listen 79.143.184.107:80;
    server_name {{ SERVER_NAME }};

{% if SSL_CERTIFICATE %}
    listen 79.143.184.107:443 ssl;

    ssl_certificate     {{ SSL_CERTIFICATE }};
    ssl_certificate_key {{ SSL_CERTIFICATE_KEY }};
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers         ALL:!ADH:RC4+RSA:+HIGH:+MEDIUM:-LOW:-SSLv2:-EXP;

    location /.well-known  {
        alias {{ BASE_DIR }}/.well-known;
    }
{% endif %}

    add_header 'Access-Control-Allow-Origin' '*';
    add_header 'Access-Control-Allow-Credentials' 'true';
    add_header 'Access-Control-Allow-Headers' 'Origin, X-Requested-With, Content-Type, Accept, X-CSRF-Token, x-csrftoken, Authorization, Keep-Alive, User-Agent, If-Modified-Since, Cache-Control, Content-Type';
    add_header 'Access-Control-Allow-Methods' 'HEAD, OPTIONS, GET, POST, PUT, DELETE, PATCH';

    location /media  {
        alias {{ BASE_DIR }}/media;
    }

    location /static  {
        alias {{ BASE_DIR }}/static;
    }


    location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host:$server_port;
        proxy_redirect off;

        if (!-f $request_filename) {
            proxy_pass http://127.0.0.1:{{ DJANGO_PORT }}; # django
            break;
        }
    }
}

[program:apolo-{{ CUSTOMER }}-gunicorn]
command = pipenv run gunicorn conf.wsgi:application --name apolo-{{ CUSTOMER }} --workers 3 --user=www-data --group=www-data --timeout 15 --log-level=info --bind=127.0.0.1:{{ DJANGO_PORT }} --log-file=-
directory = {{ BASE_DIR }}
user = www-data
stdout_logfile = /var/log/supervisor/apolo-{{ CUSTOMER }}-gunicorn.log
redirect_stderr = true

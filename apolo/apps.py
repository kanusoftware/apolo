from django.apps import AppConfig
from django.db.backends.postgresql_psycopg2.base import *


class ApoloConfig(AppConfig):
    name = 'apolo'

    def ready(self):

        # Definindo o valor padrão de IMPORT_FORMATS
        settings.IMPORT_FORMATS = getattr(settings, 'IMPORT_FORMATS', [
            'apolo.import_strategy.unimed_mensal.UnimedMensal',
            'apolo.import_strategy.unimed_servicos.UnimedServicos',
            'apolo.import_strategy.uniodonto.Uniodonto',
        ])

        # Importando os signals
        from . import signals
        signals.save_profile  # impede o formater de apagar o import não usado

        # Configurando 'icontains', 'istartswith' para pesquisar sem acento
        def _operations_lambda(self, lookup_type, internal_type=None):
            if lookup_type in ('icontains', 'istartswith'):
                return "UPPER(unaccent(%s::text))"
            else:
                return super(DatabaseOperations, self).lookup_cast(lookup_type,
                                                                   internal_type)

        DatabaseOperations.lookup_cast = _operations_lambda

        def _database_lambda(self, *args, **kwargs):
            super(DatabaseWrapper, self).__init__(*args, **kwargs)
            self.operators['icontains'] = 'LIKE UPPER(unaccent(%s))'
            self.operators['istartswith'] = 'LIKE UPPER(unaccent(%s))'
            self.ops = DatabaseOperations(self)

        DatabaseWrapper.__init__ = _database_lambda

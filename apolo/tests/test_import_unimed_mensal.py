import os
import sys

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import TestCase
from django.utils.six import StringIO

from ..models import Pessoa, Beneficiario, Extrato


class ImportUnimedMensalCriandoTest(TestCase):
    fixtures = [
        'user',
        'categoria',
        'plano',
    ]

    def setUp(self):
        self.path = os.path.join(settings.MEDIA_ROOT, 'familia_1_unimed.csv')
        self.format = 'unimed_mensal'

    def test_mensagens(self):
        out = StringIO()
        sys.stdout = out
        call_command('import', self.path, self.format, 2017, 11, stdout=out)
        self.assertIn('Total de cadastros: 5', out.getvalue())
        self.assertIn('Total de extratos: 5', out.getvalue())
        self.assertIn('Importação concluída', out.getvalue())

    def test_registros_importados(self):
        call_command('import', self.path, self.format, 2017, 11, stdout=None)
        self.assertEqual(5, Pessoa.objects.count())
        self.assertEqual(5, Beneficiario.objects.count())
        self.assertEqual(5, Extrato.objects.count())

        # Todos os importados da mesma família
        self.assertEqual(
            5,
            Beneficiario.objects.filter(cod_familia=178301).count()
        )

        # Todos os importados da mesma família
        self.assertEqual(
            5,
            Beneficiario.objects.filter(cod_familia=178301).count()
        )

        # Todos dependentes do mesmo titular
        titular = Beneficiario.objects \
            .get(cod_familia=178301, grau=Beneficiario.GRAU_TITULAR) \
            .beneficiario
        self.assertEqual(
            5,
            Beneficiario.objects
                .filter(cod_familia=178301, titular=titular)
                .count()
        )

        # Tipos de grau de parentesco
        self.assertEqual(
            1,
            Beneficiario.objects
                .filter(cod_familia=178301, grau=Beneficiario.GRAU_TITULAR)
                .count()
        )
        self.assertEqual(
            1,
            Beneficiario.objects
                .filter(cod_familia=178301, grau=Beneficiario.GRAU_CONJUGE)
                .count()
        )
        self.assertEqual(
            3,
            Beneficiario.objects
                .filter(cod_familia=178301, grau=Beneficiario.GRAU_FILHO)
                .count()
        )


class ImportUnimedMensalAtualizandoTest(TestCase):
    fixtures = [
        'user',
        'categoria',
        'plano',
        'familia_1'
    ]

    def setUp(self):
        self.path = os.path.join(settings.MEDIA_ROOT, 'familia_1_unimed.csv')
        self.format = 'unimed_mensal'

    def test_mensagens(self):
        out = StringIO()
        sys.stdout = out
        call_command('import', self.path, self.format, 2017, 11, stdout=out)
        self.assertIn('Total de cadastros: 5', out.getvalue())
        self.assertIn('Total de extratos: 5', out.getvalue())
        self.assertIn('Importação concluída', out.getvalue())

    def test_registros_importados(self):
        # Antes deve conter 5 pessoas e 5 beneficiarios
        self.assertEqual(5, Pessoa.objects.count())
        self.assertEqual(5, Beneficiario.objects.count())

        call_command('import', self.path, self.format, 2017, 11, stdout=None)

        # Apos deve conter as mesmas 5 pessoas e 5 beneficiarios
        self.assertEqual(5, Pessoa.objects.count())
        self.assertEqual(5, Beneficiario.objects.count())


class ImportUnimedMensalTentandoCriarSemTitularTest(TestCase):
    fixtures = [
        'categoria',
        'plano',
    ]

    def setUp(self):
        self.path = os.path.join(settings.MEDIA_ROOT,
                                 'familia_1_unimed_sem_titular.csv')
        self.format = 'unimed_mensal'

    def test_falha_na_importacao(self):
        with self.assertRaises(CommandError) as e:
            call_command('import', self.path, self.format, 2017, 11)
        self.assertIn('Titular da família', str(e.exception))
        self.assertIn('não encontrado', str(e.exception))

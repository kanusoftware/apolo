from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse

from apolo.services import get_competencia


class ImportCallParamsTest(TestCase):
    fixtures = [
        'user',
        'categoria',
        'plano',
    ]

    def test_post_ok(self):
        self.url = reverse('apolo:importar')
        with open(settings.MEDIA_ROOT / 'familia_1_unimed.csv', 'rb') as fp:
            self.data = {
                'competencia': get_competencia()[0][0],
                'format': 'unimed_mensal',
                'file': fp
            }
            response = self.client.post(self.url, self.data, format='multipart')
        self.assertContains(response, 'Importação concluída')

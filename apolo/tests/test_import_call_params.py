import os
import sys

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import TestCase
from django.utils.six import StringIO


class ImportCallParamsTest(TestCase):
    fixtures = [
        'user',
        'categoria',
        'plano',
    ]

    def setUp(self):
        self.path = os.path.join(settings.MEDIA_ROOT, 'familia_1_unimed.csv')
        self.format = 'unimed_mensal'

    def test_path_invalido(self):
        with self.assertRaises(CommandError) as e:
            call_command('import', 'nada.csv', self.format, 2017, 11)
            self.assertIn('não é um arquivo válido', str(e.exception))

    def test_formato_invalido(self):
        with self.assertRaises(CommandError) as e:
            call_command('import', self.path, 'nada', 2017, 11)
        self.assertIn('não é um formato configurado', str(e.exception))

    def test_mes_invalido(self):
        with self.assertRaises(CommandError) as e:
            call_command('import', self.path, self.format, 2017, 99)
        self.assertIn('não é um mês válido', str(e.exception))

        with self.assertRaises(CommandError) as e:
            call_command('import', self.path, self.format, 2017, -1)
        self.assertIn('não é um mês válido', str(e.exception))

        with self.assertRaises(CommandError) as e:
            call_command('import', self.path, self.format, 2017, 'x')
        self.assertIn('não é um mês válido', str(e.exception))

    def test_ano_invalido(self):
        with self.assertRaises(CommandError) as e:
            call_command('import', self.path, self.format, 99, 11)
        self.assertIn('não é um ano válido', str(e.exception))

        with self.assertRaises(CommandError) as e:
            call_command('import', self.path, self.format, -1, 11)
        self.assertIn('não é um ano válido', str(e.exception))

        with self.assertRaises(CommandError) as e:
            call_command('import', self.path, self.format, 'x', 11)
        self.assertIn('não é um ano válido', str(e.exception))

    def test_sucesso(self):
        out = StringIO()
        sys.stdout = out
        call_command('import', self.path, self.format, 2017, 11, stdout=out)
        self.assertIn('Importação concluída', out.getvalue())

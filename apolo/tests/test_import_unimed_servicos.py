import os
import sys

from django.conf import settings
from django.core.management import call_command
from django.db.models import Sum
from django.test import TestCase
from django.utils.six import StringIO

from ..models import Pessoa, Beneficiario, Extrato


class ImportUnimedServicosExtratosTest(TestCase):
    fixtures = [
        'user',
        'categoria',
        'plano',
        'familia_2'
    ]

    def setUp(self):
        self.path = settings.MEDIA_ROOT / 'familia_2_unimed_servicos.csv'
        self.format = 'unimed_servicos'

    def test_mensagens(self):
        out = StringIO()
        sys.stdout = out
        call_command('import', self.path, self.format, 2017, 11, stdout=out)
        self.assertIn('Total de cadastros: 5', out.getvalue())
        self.assertIn('Total de extratos: 5', out.getvalue())
        self.assertIn('Importação concluída', out.getvalue())

    def test_registros_importados(self):
        # Antes deve conter 9 pessoas e 9 beneficiarios
        self.assertEqual(9, Pessoa.objects.count())
        self.assertEqual(9, Beneficiario.objects.count())
        self.assertEqual(0, Extrato.objects.count())

        call_command('import', self.path, self.format, 2017, 11, stdout=None)

        # Apos deve conter 10 pessoas e 10 beneficiarios, e 5 extratos
        self.assertEqual(10, Pessoa.objects.count())
        self.assertEqual(10, Beneficiario.objects.count())
        self.assertEqual(5, Extrato.objects.count())

        # Valor total
        self.assertEqual(
            float(210),
            float(Extrato.objects.aggregate(v=Sum('valor'))['v'])
        )

        # Base IR
        self.assertEqual(
            float(168),
            float(Extrato.objects.aggregate(v=Sum('base_ir'))['v'])
        )

        # CONSULTA EM PRONTO SOCORRO
        self.assertEqual(
            4,
            Extrato.objects.filter(
                resumo__contains='CONSULTA EM PRONTO SOCORRO'
            ).count()
        )

        # CONSULTA EM PRONTO SOCORRO
        self.assertEqual(
            1,
            Extrato.objects.filter(
                resumo__contains='CONSULTA EM CONSULTORIO'
            ).count()
        )

        # Prestador: ELDECI CARDOSO DA SILVA
        self.assertEqual(
            1,
            Extrato.objects.filter(
                detalhe__contains='Prestador: ELDECI CARDOSO DA SILVA'
            ).count()
        )

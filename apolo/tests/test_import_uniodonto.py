import sys

from django.conf import settings
from django.core.management import call_command
from django.test import TestCase
from django.utils.six import StringIO

from ..models import Pessoa, Beneficiario, Extrato


class ImportUniodontoCriarBeneficiariosTest(TestCase):
    fixtures = [
        'user',
        'categoria',
        'plano',
    ]

    def setUp(self):
        self.path = settings.MEDIA_ROOT / 'familia_1_uniodonto.xls'
        self.format = 'uniodonto'

    def test_mensagens(self):
        out = StringIO()
        sys.stdout = out
        call_command('import', self.path, self.format, 2017, 11, stdout=out)
        self.assertIn('Total de cadastros: 5', out.getvalue())
        self.assertIn('Total de extratos: 5', out.getvalue())
        self.assertIn('Importação concluída', out.getvalue())

    def test_registros_importados(self):
        call_command('import', self.path, self.format, 2017, 11)
        self.assertEqual(5, Pessoa.objects.count())
        self.assertEqual(5, Beneficiario.objects.count())
        self.assertEqual(5, Extrato.objects.count())
        familia = '0020.0439.000131'

        # Todos os importados da mesma família
        self.assertEqual(
            5,
            Beneficiario.objects.filter(cod_familia=familia).count()
        )

        # Todos os importados da mesma família
        self.assertEqual(

            5,
            Beneficiario.objects.filter(cod_familia=familia).count()
        )

        # Todos dependentes do mesmo titular
        titular = Beneficiario.objects \
            .get(cod_familia=familia, grau=Beneficiario.GRAU_TITULAR) \
            .beneficiario
        self.assertEqual(
            5,
            Beneficiario.objects
                .filter(cod_familia=familia, titular=titular)
                .count()
        )

        # Tipos de grau de parentesco
        self.assertEqual(
            1,
            Beneficiario.objects
                .filter(cod_familia=familia, grau=Beneficiario.GRAU_TITULAR)
                .count()
        )
        self.assertEqual(
            4,
            Beneficiario.objects
                .filter(cod_familia=familia, grau=Beneficiario.GRAU_DEPENDENTE)
                .count()
        )


class ImportUniodontoUpdateBeneficiariosTest(TestCase):
    fixtures = [
        'user',
        'categoria',
        'categoria',
        'plano',
        'familia_1'
    ]

    def setUp(self):
        self.path = settings.MEDIA_ROOT / 'familia_1_uniodonto.xls'
        self.format = 'uniodonto'

    def test_mensagens(self):
        out = StringIO()
        sys.stdout = out
        call_command('import', self.path, self.format, 2017, 11, stdout=out)
        self.assertIn('Total de cadastros: 5', out.getvalue())
        self.assertIn('Total de extratos: 5', out.getvalue())
        self.assertIn('Importação concluída', out.getvalue())

    def test_registros_importados(self):
        # Antes deve conter 5 pessoas e 5 beneficiarios"
        self.assertEqual(5, Pessoa.objects.count())
        self.assertEqual(5, Beneficiario.objects.count())

        call_command('import', self.path, self.format, 2017, 11)

        # Apos deve conter as mesmas 5 pessoas e 10 beneficiarios
        self.assertEqual(5, Pessoa.objects.count())
        self.assertEqual(10, Beneficiario.objects.count())

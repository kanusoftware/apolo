from django.urls import path

from . import views

app_name = 'apolo'

urlpatterns = [
    path('importar/', views.Importar.as_view(), name='importar'),
]

import tempfile
from django.contrib import messages
from django.views.generic import FormView

from . import forms
from . import services


class Importar(FormView):
    form_class = forms.Importar
    template_name = 'apolo/importar.html'

    def form_valid(self, form):
        # try:
        y, m, _ = form.cleaned_data['competencia'].split('-')


        # create a temporary file and write some data to it
        fp = tempfile.mkstemp()
        fp.write(b'bla')
        fp.close()

        with open('/tmp/bla.csv', 'wb') as f:
            content = form.cleaned_data['file'].read()
            f.write(content)

        cadastros, extratos = services.importar(
            self.request.FILES['file'],
            # '/tmp/bla.csv',
            # form.cleaned_data['file'],
            form.cleaned_data['format'],
            y,
            m
        )

        messages.success(self.request, form.cleaned_data)
        # except Exception as e:
        #     messages.error(self.request, str(e))

        return self.get(self.request)

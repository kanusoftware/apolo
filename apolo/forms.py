from django import forms
from django.contrib.admin.widgets import AdminDateWidget
from django_select2.forms import Select2Widget, ModelSelect2Widget

from . import models
from . import services


class CustonAdminDateWidget(AdminDateWidget):
    """
    AdminDateWidget não estava funcionando com o autocomplete de utilizador
    """

    @property
    def media(self):
        js = [
            'vendor/jquery/jquery.min.js',
            'jquery.init.js',
            'calendar.js',
            'admin/DateTimeShortcuts.js',
        ]
        return forms.Media(js=["admin/js/%s" % path for path in js])


class ExtratoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExtratoForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance')

        self.initial['calcular_ir'] = True
        if instance:
            self.initial['calcular_ir'] = instance.base_ir > 0

    def save(self, commit=True):
        if self.cleaned_data['calcular_ir']:
            self.instance.base_ir = self.instance.valor
        else:
            self.instance.base_ir = 0

        return super(ExtratoForm, self).save(commit)

    utilizador = forms.ModelChoiceField(
        queryset=models.Beneficiario.objects.all(),
        widget=ModelSelect2Widget(
            model=models.Beneficiario,
            search_fields=['beneficiario__nome__icontains'],
            dependent_fields={'plano': 'plano'},
            max_results=20,
            attrs={
                'data-placeholder': 'Procurar beneficiário',
                'data-width': '30em',
                'data-language': 'pt-BR',
            }
        )
    )

    calcular_ir = forms.BooleanField(label='IR sobre o valor?', initial=True,
                                     help_text='Valor entra no cálculo do IR?',
                                     required=False)
    dtevento = forms.DateField(required=True, label='Data evento',
                               widget=CustonAdminDateWidget())
    competencia = forms.ChoiceField(choices=services.get_competencia(),
                                    help_text='Mês onde será lançado')

    class Meta:
        model = models.Extrato
        fields = ('plano', 'utilizador', 'resumo', 'detalhe', 'valor',
                  'dtevento', 'competencia', 'calcular_ir',)

        widgets = {
            'plano': Select2Widget,
        }


class Importar(forms.Form):
    format = forms.ChoiceField(label='Formato do arquivo', required=True,
                               choices=[],
                               help_text='Mês onde será lançado')

    competencia = forms.ChoiceField(label='Competência', required=True,
                                    choices=services.get_competencia(),
                                    help_text='Mês onde será lançado')
    file = forms.FileField(label='Arquivo para importação', required=True)

    def __init__(self, **kwargs):
        super(Importar, self).__init__(**kwargs)

        self.fields['format'].choices = services.get_format_choices()

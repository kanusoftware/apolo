from datetime import date
from typing import List, Type

from dataclasses import dataclass, field

from apolo.models import Plano


@dataclass
class Extrato:
    resumo: str
    detalhe: str
    valor: float
    base_ir: float
    dtevento: date = None


@dataclass
class Beneficiario:
    plano: Type[Plano]
    nome: str
    grau: str
    cpf: str = None
    cod_beneficiario: str = None
    cod_familia: str = None
    extratos: List[Extrato] = field(default_factory=Extrato)

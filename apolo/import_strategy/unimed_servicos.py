from datetime import datetime as dt

import pandas as pd

from apolo import models
from . import value_objects as vo


class UnimedServicos:
    """
    Processamento da planilha de serviços prestados da Unimed
    """
    name = 'unimed_servicos'
    desc = 'Unimed Serviços'

    def run(self, file_path):
        df: pd.DataFrame = None
        cols = [4, 7, 8, 9, 12, 13, 18, 22, 24, 26]
        encodings = ['utf-8', 'iso-8859-1']
        for e in encodings:
            try:
                df = pd.read_csv(file_path, usecols=cols, encoding=e)
            except UnicodeDecodeError:
                pass

        # Definindo o nome das colunas
        df.columns = [
            'COD_FAMILIA',
            'COD_BENEFICIARIO',
            'NOME',
            'GRAU_DEPENDENCIA',
            'EMERGENCIA',
            'DT_ATENDIMENTO',
            'PRESTADOR',
            'DESCRICAO_SERVICO',
            'VALOR',
            'BASE_IR',
        ]

        # Filtrando linhas que 'NOME' não é null
        df = df[pd.notnull(df['NOME'])]

        # Transformação de tipos
        df['VALOR'] = df['VALOR'].astype(float)
        df['BASE_IR'] = df['BASE_IR'].astype(float)

        # Criando os VOs
        result = []
        plano = vo.Plano.objects.get(slug='unimed')
        for index, row in df.iterrows():
            detalhe = f"{row['DESCRICAO_SERVICO']}\n" \
                      f"Emergência: {row['EMERGENCIA']}\n" \
                      f"Prestador: {row['PRESTADOR']}"

            extrato = vo.Extrato(
                resumo=row['DESCRICAO_SERVICO'],
                detalhe=detalhe,
                valor=float(row['VALOR']),
                base_ir=float(row['BASE_IR']),
                dtevento=dt.strptime(row['DT_ATENDIMENTO'], '%d/%m/%Y').date()
            )

            beneficiario = vo.Beneficiario(
                plano=plano,
                nome=row['NOME'],
                grau=self.get_grau(row['GRAU_DEPENDENCIA']),
                cod_beneficiario=row['COD_BENEFICIARIO'],
                cod_familia=row['COD_FAMILIA'],
                extratos=[extrato],
                cpf=None
            )
            result.append(beneficiario)

        return result

    def get_grau(self, grau):
        if grau == 'TITULAR':
            return models.Beneficiario.GRAU_TITULAR
        elif grau == 'CONJUGE':
            return models.Beneficiario.GRAU_CONJUGE
        elif grau == 'FILHOS (AS)':
            return models.Beneficiario.GRAU_FILHO
        elif grau == 'AGREGADOS':
            return models.Beneficiario.GRAU_AGREGADO

        raise Exception('Grau de dependência "%s" não definido' % grau)

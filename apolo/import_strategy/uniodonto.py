from datetime import date

import pandas as pd

from apolo import models
from . import value_objects as vo


class Uniodonto:
    """
    Processamento da planilha de mensalidades da Uniodonto
    """

    name = 'uniodonto'
    desc = 'Uniodonto'

    def run(self, file_path):
        df: pd.DataFrame = None
        cols = [6, 8, 9, 10, 15, 16, 20]
        encodings = ['utf-8', 'iso-8859-1']
        for e in encodings:
            try:
                df = pd.read_excel(file_path, usecols=cols, encoding=e)
            except UnicodeDecodeError:
                pass

        # Definindo o nome das colunas
        df.columns = [
            'COD_BENEFICIARIO',
            'GRAU_DEPENDENCIA',
            'NOME',
            'CPF',
            'MES_FATURAMENTO',
            'ANO_FATURAMENTO',
            'VALOR'
        ]

        # Filtrando linhas que 'NOME' não é null
        df = df[pd.notnull(df['NOME'])]

        # Transformação de tipos
        df[['ANO_FATURAMENTO']] = df[['ANO_FATURAMENTO']].astype(int)
        df[['MES_FATURAMENTO']] = df[['MES_FATURAMENTO']].astype(int)

        df['VALOR'] = [
            v.replace('.', '').replace(',', '.')
            for v in df['VALOR']
        ]
        df['COD_FAMILIA'] = [v.split('-')[0] for v in df['COD_BENEFICIARIO']]
        df['CPF'] = [str(v).split('.')[0] for v in df['CPF']]

        # Criando os VOs
        result = []
        plano = vo.Plano.objects.get(slug='uniodonto')
        for index, row in df.iterrows():
            extrato = vo.Extrato(
                resumo='Mensalidade Uniodonto',
                detalhe='Mensalidade Uniodonto',
                valor=float(row['VALOR']),
                base_ir=float(row['VALOR']),
                dtevento=date(year=row['ANO_FATURAMENTO'],
                              month=row['MES_FATURAMENTO'],
                              day=1)
            )

            beneficiario = vo.Beneficiario(
                plano=plano,
                nome=row['NOME'],
                grau=self.get_grau(row['GRAU_DEPENDENCIA']),
                cpf=row['CPF'],
                cod_beneficiario=row['COD_BENEFICIARIO'],
                cod_familia=row['COD_FAMILIA'],
                extratos=[extrato]
            )
            result.append(beneficiario)

        return result

    def get_grau(self, grau):
        if grau == 'T':
            return models.Beneficiario.GRAU_TITULAR
        elif grau == 'D':
            return models.Beneficiario.GRAU_DEPENDENTE

        raise Exception('Grau de dependência "%s" não definido' % grau)

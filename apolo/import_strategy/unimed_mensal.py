from io import BytesIO

import pandas as pd

from apolo import models
from . import value_objects as vo


class UnimedMensal:
    """
    Processamento da planilha de mensalidades e inscrições da Unimed
    """

    name = 'unimed_mensal'
    desc = 'Unimed Mensal'

    def run(self, file):
        df: pd.DataFrame = None
        cols = [7, 8, 12, 13, 17]
        encodings = ['utf-8', 'iso-8859-1']
        for e in encodings:
            try:
                df = pd.read_csv(file, usecols=cols, encoding=e,lineterminator='\n')
            except UnicodeDecodeError as e:
                print(e)
                pass

        # Definindo o nome das colunas
        df.columns = [
            'COD_FAMILIA',
            'GRAU_DEPENDENCIA',
            'COD_BENEFICIARIO',
            'NOME',
            'VALOR'
        ]

        # Filtrando linhas que 'NOME' não é null
        df = df[pd.notnull(df['NOME'])]

        # Transformação de tipos
        df['VALOR'] = df['VALOR'].astype(float)

        # Criando os VOs
        result = []
        plano = vo.Plano.objects.get(slug='unimed')
        for index, row in df.iterrows():
            extrato = vo.Extrato(
                resumo='Mensalidade Unimed',
                detalhe='Mensalidade Unimed',
                valor=float(row['VALOR']),
                base_ir=float(row['VALOR'])
            )

            beneficiario = vo.Beneficiario(
                plano=plano,
                nome=row['NOME'],
                grau=self.get_grau(row['GRAU_DEPENDENCIA']),
                cpf=None,
                cod_beneficiario=row['COD_BENEFICIARIO'],
                cod_familia=row['COD_FAMILIA'],
                extratos=[extrato]
            )
            result.append(beneficiario)

        return result

    def get_grau(self, grau):
        if grau == 'TITULAR':
            return models.Beneficiario.GRAU_TITULAR
        elif grau == 'CONJUGE':
            return models.Beneficiario.GRAU_CONJUGE
        elif grau == 'FILHOS (AS)':
            return models.Beneficiario.GRAU_FILHO
        elif grau == 'AGREGADOS':
            return models.Beneficiario.GRAU_AGREGADO

        raise Exception('Grau de dependência "%s" não definido' % grau)

from django.contrib.auth import get_user_model

from django.db import models


class Auditoria(models.Model):
    created_by = models.ForeignKey(get_user_model(), on_delete=models.PROTECT,
                                   related_name='+', verbose_name='Criado por',
                                   null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True,
                                      verbose_name='Criado em',
                                      null=True, blank=True)
    modified_by = models.ForeignKey(get_user_model(), on_delete=models.PROTECT,
                                    related_name='+',
                                    verbose_name='Modificado por',
                                    null=True, blank=True)
    modified_on = models.DateTimeField(auto_now=True,
                                       verbose_name='Modificado em',
                                       null=True, blank=True)

    class Meta:
        abstract = True


class Categoria(models.Model):
    nome = models.CharField(max_length=255)
    padrao = models.BooleanField(default=False, verbose_name='Padrão')

    def save(self, *args, **kwargs):
        # Desmarca qualquer outro que seja padrao=True
        if self.padrao:
            qs = Categoria.objects.all()
            if self.pk:
                qs = qs.filter(pk__isnot=self.pk)
            qs.update(padrao=False)

        return super(Categoria, self).save(*args, **kwargs)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'


class Plano(models.Model):
    nome = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Plano'
        verbose_name_plural = 'Planos'


class Pessoa(models.Model):
    nome = models.CharField(max_length=255)
    cpf = models.CharField(max_length=11, null=True)
    categoria = models.ForeignKey(Categoria, on_delete=models.PROTECT)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Pessoa'
        verbose_name_plural = 'Pessoas'


class Beneficiario(models.Model):
    GRAU_TITULAR = 'TITULAR'
    GRAU_AGREGADO = 'AGREGADOS'
    GRAU_COMPANHEIRO = 'COMPANHEIRO(A)'
    GRAU_CONJUGE = 'CONJUGE'
    GRAU_FILHO = 'FILHOS(AS)'
    GRAU_DEPENDENTE = 'DEPENDENTES'
    _GRAU_CHOICES = (
        (GRAU_TITULAR, 'Titular'),
        (GRAU_AGREGADO, 'Agregados'),
        (GRAU_COMPANHEIRO, 'Companheiro(a)'),
        (GRAU_CONJUGE, 'Conjuge'),
        (GRAU_FILHO, 'Filho(a)'),
        (GRAU_DEPENDENTE, 'Dependente'),
    )
    plano = models.ForeignKey(Plano, on_delete=models.PROTECT)
    grau = models.CharField(max_length=14, choices=_GRAU_CHOICES,
                            verbose_name='Grau parentesco')
    data_cadastro = models.DateField('Dt cadastro', auto_now_add=True)
    cod_beneficiario = models.CharField(max_length=25,
                                        verbose_name='Cód. beneficiário')
    cod_familia = models.CharField(max_length=25, verbose_name='Cód. família')
    beneficiario = models.ForeignKey(Pessoa, verbose_name='Beneficiário',
                                     related_name='beneficiario',
                                     on_delete=models.PROTECT)
    titular = models.ForeignKey(Pessoa, related_name='titular',
                                on_delete=models.PROTECT)

    def __str__(self):
        return self.beneficiario.nome

    class Meta:
        verbose_name = 'Beneficiário'
        verbose_name_plural = 'Beneficiários'


class Extrato(Auditoria):
    plano = models.ForeignKey(Plano, on_delete=models.PROTECT)
    utilizador = models.ForeignKey(Beneficiario, verbose_name='Utilizador',
                                   on_delete=models.PROTECT)
    resumo = models.CharField(max_length=50)
    detalhe = models.TextField()
    valor = models.DecimalField(max_digits=8, decimal_places=2)
    base_ir = models.DecimalField(max_digits=8, decimal_places=2)
    dtevento = models.DateField('Data', null=True, blank=True)
    competencia = models.DateField('Competência')

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Lançamento'
        verbose_name_plural = 'Lançamentos'


class Competencia(models.Model):
    nome = models.CharField(max_length=255)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Competência'
        verbose_name_plural = 'Competências'

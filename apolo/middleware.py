from django.contrib.auth import get_user_model
from django.utils.deprecation import MiddlewareMixin

_current_user = None


def get_current_user_or_admin():
    global _current_user
    # t = current_thread()
    #
    # if t not in _requests:
    #     return None
    if not _current_user or _current_user.is_anonymous:
        _current_user = get_user_model().objects.get(pk=1)

    return _current_user


class RequestMiddleware(MiddlewareMixin):
    # def __init__(self, get_response):
    #     self.get_response = get_response
    #     # One-time configuration and initialization.
    #
    # def __call__(self, request):
    #     _requests[current_thread()] = request
    #     return request
    def process_request(self, request):
        global _current_user
        _current_user = request.user

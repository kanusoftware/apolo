import os
from datetime import date
from typing import List, ClassVar

from dateutil.relativedelta import *
from django.conf import settings
from django.db.models import Q
from django.utils.module_loading import import_string

from apolo.import_strategy import value_objects as vo
from apolo.models import Pessoa, Beneficiario, Extrato


def get_competencia() -> List:
    """
    Devolve uma lista com os meses de competência de acordo com a configuração
    :return:
    """
    item_list = []
    current = date.today().replace(day=1)  # 1o dia do mês atual
    for i in range(0, -12, -1):
        relative = relativedelta(months=i)
        dt = current + relative
        item_list.append((dt.strftime('%Y-%m-%d'), dt.strftime('%m/%Y')))

    return item_list


def get_format_choices() -> List:
    """
    Devolve uma lista com os formatos configurados para usar em formulários
    :return:
    """
    choices = []
    for class_name in settings.IMPORT_FORMATS:
        cl = import_string(class_name)
        choices.append((cl.name, cl.desc))

    return choices


def get_format_class(format_name) -> ClassVar:
    """
    Devolve a classe de importação com nome format_name ou um exception
    :return:
    """

    for class_name in settings.IMPORT_FORMATS:
        cl = import_string(class_name)
        if cl.name == format_name:
            return cl
    else:
        raise Exception(f'"{format_name}" não é um formato configurado')


class Importar():
    """
    Importar arquivos de mensalidade conforme formato
    """

    def handle(self, file, format, year, month):
        # Valida existência do arquivo
        # if not os.path.isfile(file):
        #     error = '"%s" não é um arquivo válido' % options['file_path']
        #     raise Exception(error)

        # Valida o formato
        try:
            # Carregando a extratégia de processamento
            strategy = get_format_class(format)()
        except Exception as e:
            raise Exception(e)

        # Valida o Mês
        try:
            month = int(month)
            assert month >= 1
            assert month <= 12
        except Exception:
            raise Exception(f'{month} não é um mês válido')

        # Valida o Ano
        try:
            current_year = int(date.today().strftime('%Y'))
            year = int(year)
            assert year >= current_year - 5
            assert year <= current_year
        except Exception:
            raise Exception(f'{year} não é um ano válido' )

        try:
            # Tratando arquivo
            value_objects = self.process_file(file, strategy)

            # Tratando VOs encontrados
            cadastros, extratos = self.process_vo(value_objects, year, month)
        except Exception as e:
            raise Exception(f'Falha ao processar estratégia: {e}')

        msg = 'Total de cadastros: %s' % cadastros
        self.stdout.write(self.style.SUCCESS(msg))

        msg = 'Total de extratos: %s' % extratos
        self.stdout.write(self.style.SUCCESS(msg))

        # msg = 'Importação concluída'
        # self.stdout.write(self.style.SUCCESS(msg))

    def process_file(self, file_path, strategy):
        """
        Trata arquivo individual

        :param file_path: Endereço em disco do arquivo
        :param strategy: Estratégia de processamento do arquivo
        :return: list[cadastros[extratos]]
        """
        return strategy.run(file_path)

    def process_vo(self, value_objects: list, year: int, month: int):
        """
        Trata a lista de vos

        :param value_objects:
        :param year:
        :param month:
        :return: int, int
        """
        cadastros = len(value_objects)
        extratos = 0

        for entity in value_objects:
            # Contabilizando os extratos
            extratos += len(entity.extratos)

            # Tratando beneficiários
            beneficiario, titular = self.get_or_create_beneficiario(entity)

            # Tratando Extratos
            for vo in entity.extratos:
                Extrato.objects.create(
                    utilizador=beneficiario,
                    plano=beneficiario.plano,
                    resumo=vo.resumo,
                    detalhe=vo.detalhe,
                    valor=vo.valor,
                    base_ir=vo.base_ir,
                    dtevento=vo.dtevento,
                    competencia=date(year=year, month=month, day=1)
                )

        return cadastros, extratos

    def get_or_create_beneficiario(self, vo: vo.Beneficiario):
        """
        Encontra ou cria o beneficiário se este for novo

        :param entity:
        :return:
        """
        # Tratando Pessoa
        try:
            search = Q(nome=vo.nome)
            if vo.cpf:
                search = search | Q(cpf=vo.cpf)

            pessoa = Pessoa.objects.get(search)

            # Pessoa encontrada por nome, e entidade com cpf
            if not pessoa.cpf and vo.cpf:
                pessoa.cpf = vo.cpf
                pessoa.save()
        except Pessoa.DoesNotExist:
            pessoa = Pessoa.objects.create(
                nome=vo.nome,
                cpf=vo.cpf,
                categoria=self.categoria_padrao
            )

        # Encontra titular da familia
        titular = None
        try:
            titular = Beneficiario.objects.get(
                cod_familia=vo.cod_familia,
                grau=Beneficiario.GRAU_TITULAR
            )
        except Beneficiario.DoesNotExist:
            # Se encontrou o titular, e o atual nao for o titular
            if not vo.grau == Beneficiario.GRAU_TITULAR:
                raise Exception(
                    f'Titular da família {vo.cod_familia} não encontrado'
                )

        # Tratando Beneficiario
        beneficiario, _ = Beneficiario.objects.get_or_create(
            defaults={
                'plano': vo.plano,
                'grau': vo.grau,
                'cod_beneficiario': vo.cod_beneficiario,
                'cod_familia': vo.cod_familia,
                'beneficiario': pessoa,
                'titular': titular.titular if titular else pessoa
            },
            beneficiario=pessoa,
            cod_beneficiario=vo.cod_beneficiario
        )
        return beneficiario, titular or beneficiario

    def __call__(self, file, format, year, month):
        return self.handle(file, format, year, month)


importar = Importar()

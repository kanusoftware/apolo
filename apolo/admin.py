from django.contrib import admin
from django.db.models import Q
from rangefilter.filter import DateRangeFilter

from . import forms
from . import models
from . import services


class CompetenciaListFilter(admin.SimpleListFilter):
    """
    Filtra por competência
    """
    title = 'Competência'
    parameter_name = 'competencia'
    default_value = None

    def lookups(self, request, model_admin):
        """
        Devolve opções para o selectbox
        """
        return services.get_competencia()

    def queryset(self, request, queryset):
        """
        Filtra o queryset do ModelAdmin
        """
        y, m, _ = self.value().split('-')
        return queryset.filter(competencia__month=m, competencia__year=y)

    def value(self):
        """
        Devolve o primeiro valor (Mês atual)
        """
        value = super(CompetenciaListFilter, self).value()
        if value is None:
            item_list = self.lookups(None, None)
            return item_list.pop(0)[0]
        return str(value)


class InputFilter(admin.SimpleListFilter):
    template = 'admin/input_filter.html'

    def lookups(self, request, model_admin):
        # Dummy, required to show the filter.
        return (('', ''),)


class CPFFilter(InputFilter):
    parameter_name = 'cpf'
    title = 'CPF'

    def queryset(self, request, queryset):
        if self.value() is not None:
            val = self.value()
            return queryset.filter(
                Q(utilizador__beneficiario__cpf=val) |
                Q(utilizador__titular__cpf=val)
            )


class NomeFilter(InputFilter):
    parameter_name = 'nome'
    title = 'Nome'

    def queryset(self, request, queryset):
        if self.value() is not None:
            val = self.value()
            return queryset.filter(
                Q(utilizador__beneficiario__nome__icontains=val) |
                Q(utilizador__titular__nome__icontains=val)
            )


class ExtratoAdmin(admin.ModelAdmin):
    # change_form_template = 'apolo/extrato.html'
    form = forms.ExtratoForm
    list_select_related = [
        'utilizador',
        'utilizador__beneficiario',
        'utilizador__titular',
    ]
    list_display = [
        'utilizador', 'titular', 'resumo', 'valor', 'dtevento', 'plano',
        'cod_familia',
    ]
    list_filter = [
        CPFFilter,
        NomeFilter,
        CompetenciaListFilter,
        'plano',
        ('dtevento', DateRangeFilter),
    ]

    def cod_familia(self, entity):
        return entity.utilizador.cod_familia

    cod_familia.short_description = 'Cód. Família'

    # def beneficiario(self, entity):
    #     return entity.utilizador.beneficiario.nome

    def titular(self, entity):
        if entity.utilizador.beneficiario_id == entity.utilizador.titular_id:
            return '-'
        return entity.utilizador.titular.nome

        # list_per_page = 20


from admin_extra_urls.extras import ExtraUrlMixin, link, action


class PessoaAdmin(ExtraUrlMixin, admin.ModelAdmin):
    @link()  # /admin/myapp/mymodel/update_all/
    def update_all(self, request):
        raise Exception('fazendo algo para todos')

    @action()  # /admin/myapp/mymodel/update/10/
    def update(self, request, pk):
        obj = self.get_object(request, pk)
        raise Exception(f'fazendo algo para {obj}')


admin.site.register(models.Pessoa, PessoaAdmin)
admin.site.register(models.Beneficiario)
admin.site.register(models.Categoria)
admin.site.register(models.Plano)
admin.site.register(models.Extrato, ExtratoAdmin)

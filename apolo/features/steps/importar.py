from behave import given, when, then


@given(u'um cliente chamado "{nome}"')
def step_impl(context, nome):
    pass



@given(u'um administrador global chamado "{nome}"')
def step_impl(context, nome):
    browser=context.browser
    browser.visit('http://www.google.com')

    browser.fill('q', nome)
    # browser.driver.find_element_by_css_selector('[name=btnK]').click()
    # browser.find_by_name('btnK').first.click()
    browser.find_by_css('input[value="Pesquisa Google"]').first.click()
    context.nome = nome
    # assert nome == 'Greg'


@given(u'um blog chamado "{blog}" de propriedade de "{nome}"')
def step_impl(context, blog, nome):
    # assert True == False
    pass


@given(u'que eu esteja logado como {nome}')
def step_impl(context, nome):
    pass


@when(u'eu tentar postar em "{blog}"')
def step_impl(context, blog):
    pass


@then(u'eu devo ver "Seu artigo foi publicado"')
def step_impl(context):
    pass

@given(u'que as seguintes pessoas existem')
def step_impl(context):
    assert 'Aproximadamente' in context.response
    # for row in context.table:
    #     raise Exception(row[0],row[1],row[2])
    pass
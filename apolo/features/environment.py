import os

from splinter import Browser


def before_all(context):
    kwargs = {}
    if os.environ.get('BROWSER_NAME') == 'chrome':
        kwargs = {
            'headless': True
        }

    context.browser = Browser(os.environ.get('BROWSER_NAME'), **kwargs, )


def after_all(context):
    context.browser.quit()

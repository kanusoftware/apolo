# language: pt
Funcionalidade: Suporte a múltiplos sites

  Contexto:
    Dado um administrador global chamado "Greg"
    E um blog chamado "Greg esbraveja contra impostos" de propriedade de "Wilson"
    E um cliente chamado "Wilson"
    E um blog chamado "Terapia Cara" de propriedade de "Wilson"
    Dado que as seguintes pessoas existem:
    | nome  | email           | fone  |
    | Aslak | aslak@email.com | 123   |
    | Joe   | joe@email.com   | 234   |
    | Bryan | bryan@email.org | 456   |


  Cenário: Greg posta no blog de um cliente
      Dado que eu esteja logado como Greg
      Quando eu tentar postar em "Terapia Cara"
      Então eu devo ver "Seu artigo foi publicado"

  Cenário: Wilson posta em seu próprio blog
      Dado que eu esteja logado como Wilson
      Quando eu tentar postar em "Terapia Cara"
      Então eu devo ver "Seu artigo foi publicado"

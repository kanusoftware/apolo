from django.db.models.signals import pre_save
from django.dispatch import receiver

from . import models
from .middleware import get_current_user_or_admin


@receiver(pre_save, sender=models.Extrato)
def save_profile(sender, instance, **kwargs):
    instance.modified_by = get_current_user_or_admin()

    if not instance.created_by and instance._state.adding:
        instance.created_by = get_current_user_or_admin()


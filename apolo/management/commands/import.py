"""
Gestor de importação
"""
import os
import sys
from datetime import date

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from apolo.import_strategy import value_objects as vo
from apolo.models import Categoria, Pessoa, Beneficiario, Extrato
from apolo.services import get_format_class


class Command(BaseCommand):
    help = 'Importar arquivos de mensalidade conforme formato'
    testing = 'test' in sys.argv

    try:
        categoria_padrao = Categoria.objects.get(padrao=True)
    except Categoria.DoesNotExist:
        raise CommandError('Nenhuma categoria cadastrada como padrão')

    def add_arguments(self, parser):
        parser.add_argument(
            'file_path',
            type=str,
            help='Arquivo para importação'
        )
        parser.add_argument(
            'format',
            type=str,
            help='Formato do arquivo'
        )
        parser.add_argument(
            'year',
            type=str,
            help='Ano do extrato'
        )
        parser.add_argument(
            'month',
            type=str,
            help='Mês do extrato'
        )

    def handle(self, *args, **options):
        """
        Função principal do comando

        :param args:
        :param options:
        :return:
        """

        # Valida existência do arquivo
        if not os.path.isfile(options['file_path']):
            error = '"%s" não é um arquivo válido' % options['file_path']
            raise CommandError(self.style.ERROR(error))

        # Valida o formato
        try:
            # Carregando a extratégia de processamento
            strategy = get_format_class(options['format'])()
        except Exception as e:
            raise CommandError(self.style.ERROR(e))

        # Valida o Mês
        try:
            month = int(options['month'])
            assert month >= 1
            assert month <= 12
        except Exception:
            error = '"%s" não é um mês válido' % options['month']
            raise CommandError(self.style.ERROR(error))

        # Valida o Ano
        try:
            current_year = int(date.today().strftime('%Y'))
            year = int(options['year'])
            assert year >= current_year - 5
            assert year <= current_year
        except Exception:
            error = '"%s" não é um ano válido' % options['year']
            raise CommandError(self.style.ERROR(error))

        try:
            # Tratando arquivo
            value_objects = self.process_file(options['file_path'], strategy)

            # Tratando VOs encontrados
            cadastros, extratos = self.process_vo(value_objects, year, month)
        except Exception as e:
            error = 'Falha ao processar estratégia: "%s"' % e
            raise CommandError(self.style.ERROR(error))

        msg = 'Total de cadastros: %s' % cadastros
        self.stdout.write(self.style.SUCCESS(msg))

        msg = 'Total de extratos: %s' % extratos
        self.stdout.write(self.style.SUCCESS(msg))

        msg = 'Importação concluída'
        self.stdout.write(self.style.SUCCESS(msg))

    def process_file(self, file_path, strategy):
        """
        Trata arquivo individual

        :param file_path: Endereço em disco do arquivo
        :param strategy: Estratégia de processamento do arquivo
        :return: list[cadastros[extratos]]
        """
        return strategy.run(file_path)

    def process_vo(self, value_objects: list, year: int, month: int):
        """
        Trata a lista de vos

        :param value_objects:
        :param year:
        :param month:
        :return: int, int
        """
        cadastros = len(value_objects)
        extratos = 0

        for entity in value_objects:
            # Contabilizando os extratos
            extratos += len(entity.extratos)

            # Tratando beneficiários
            beneficiario, titular = self.get_or_create_beneficiario(entity)

            # Tratando Extratos
            for vo in entity.extratos:
                Extrato.objects.create(
                    utilizador=beneficiario,
                    plano=beneficiario.plano,
                    resumo=vo.resumo,
                    detalhe=vo.detalhe,
                    valor=vo.valor,
                    base_ir=vo.base_ir,
                    dtevento=vo.dtevento,
                    competencia=date(year=year, month=month, day=1)
                )

        return cadastros, extratos

    def get_or_create_beneficiario(self, vo: vo.Beneficiario):
        """
        Encontra ou cria o beneficiário se este for novo

        :param entity:
        :return:
        """
        # Tratando Pessoa
        try:
            search = Q(nome=vo.nome)
            if vo.cpf:
                search = search | Q(cpf=vo.cpf)

            pessoa = Pessoa.objects.get(search)

            # Pessoa encontrada por nome, e entidade com cpf
            if not pessoa.cpf and vo.cpf:
                pessoa.cpf = vo.cpf
                pessoa.save()
        except Pessoa.DoesNotExist:
            pessoa = Pessoa.objects.create(
                nome=vo.nome,
                cpf=vo.cpf,
                categoria=self.categoria_padrao
            )

        # Encontra titular da familia
        titular = None
        try:
            titular = Beneficiario.objects.get(
                cod_familia=vo.cod_familia,
                grau=Beneficiario.GRAU_TITULAR
            )
        except Beneficiario.DoesNotExist:
            # Se encontrou o titular, e o atual nao for o titular
            if not vo.grau == Beneficiario.GRAU_TITULAR:
                raise Exception(
                    f'Titular da família {vo.cod_familia} não encontrado'
                )

        # Tratando Beneficiario
        beneficiario, _ = Beneficiario.objects.get_or_create(
            defaults={
                'plano': vo.plano,
                'grau': vo.grau,
                'cod_beneficiario': vo.cod_beneficiario,
                'cod_familia': vo.cod_familia,
                'beneficiario': pessoa,
                'titular': titular.titular if titular else pessoa
            },
            beneficiario=pessoa,
            cod_beneficiario=vo.cod_beneficiario
        )
        return beneficiario, titular or beneficiario
